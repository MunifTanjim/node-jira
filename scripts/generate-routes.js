const _ = require('lodash')
const path = require('path')
const { writeFileSync } = require('fs')

const deepclean = require('clean-deep')
const deepsort = require('deep-sort-object')
const deepmerge = require('../src/utils/deepmerge.js')

const { camelCase, pascalCase } = require('./helpers.js')

const API_NAMES = require('../routes/api-names.json')
const PATHS_SPEC = require('../specification/paths.json')

const PATHS_SPEC_EXTRAS = require('../specification/extras/paths.json')

const routesPath = path.resolve('routes/routes.json')

const routesObject = {}

const initializeRoutes = routesObject => {
  _.each(API_NAMES, (methods, url) => {
    _.each(methods, (apiName, method) => {
      apiName = camelCase(apiName)

      routesObject[apiName] = {
        params: {}
      }
    })
  })
}

const setBodyType = (apiObject, { requestBody }) => {
  if (!requestBody) {
    return
  }

  let bodyType

  if (requestBody.$ref) {
    bodyType = requestBody.$ref
  } else if (requestBody.content) {
    if (
      !requestBody.content['application/json'] ||
      !requestBody.content['application/json'].schema
    ) {
      return
    }

    bodyType = requestBody.content['application/json'].schema.$ref
  }

  if (!bodyType) {
    return
  }

  apiObject.bodyType = pascalCase(bodyType.replace(/#\/components\/\w+?\//, ''))
}

const setHTTPMethod = (apiObject, method) => {
  apiObject.method = method.toUpperCase()
}

const setParameters = (apiObject, { parameters = [] }) => {
  _.each(
    parameters,
    ({
      in: _in,
      name,
      required,
      schema: { enum: _enum, items, type = 'any' }
    }) => {
      if (!apiObject.params[name]) {
        apiObject.params[name] = {}
      }

      apiObject.params[name] = deepmerge(apiObject.params[name], {
        enum: _enum,
        in: _in,
        type
      })

      if (items && items.type) {
        apiObject.params[name].itemsType = items.type
      }

      if (required) {
        apiObject.params[name].required = required
      }

      apiObject.params[name].enum = _.uniq(apiObject.params[name].enum)
    }
  )
}

const setReturnTypes = (apiObject, { responses }) => {
  _.each(responses, (response, code) => {
    if (Number(code) < 400) {
      if (
        !response.content ||
        !response.content['application/json'] ||
        !response.content['application/json'].schema ||
        !response.content['application/json'].schema.$ref
      ) {
        return
      }

      apiObject.responseType = pascalCase(
        response.content['application/json'].schema.$ref.replace(
          /#\/components\/\w+?\//,
          ''
        )
      )
    }
  })
}

const setURL = (apiObject, url) => {
  apiObject.url = url
}

const updateRoutes = routesObject => {
  _.each(API_NAMES, (methods, url) => {
    let spec = PATHS_SPEC[url] || {}
    let specExtras = PATHS_SPEC_EXTRAS[url] || {}

    _.each(methods, (apiName, method) => {
      if (!apiName) return

      apiName = camelCase(apiName)

      setHTTPMethod(routesObject[apiName], method)
      setURL(routesObject[apiName], url)

      if (spec[method]) {
        setBodyType(routesObject[apiName], spec[method])
        setParameters(routesObject[apiName], spec[method])
        setReturnTypes(routesObject[apiName], spec[method])
      }

      if (specExtras[method]) {
        setBodyType(routesObject[apiName], specExtras[method])
        setParameters(routesObject[apiName], specExtras[method])
        setReturnTypes(routesObject[apiName], specExtras[method])
      }
    })
  })
}

initializeRoutes(routesObject)
updateRoutes(routesObject)

writeFileSync(
  routesPath,
  `${JSON.stringify(deepsort(deepclean(routesObject)), null, 2)}\n`
)
