const _ = require('lodash')
const path = require('path')
const Mustache = require('mustache')
const { readFileSync, writeFileSync } = require('fs')

const { pascalCase, getBadSchemaKeys } = require('./helpers.js')

const ROUTES = require('../routes/routes.json')

const typesMap = Object.assign(
  { integer: 'number' },
  {
    ComAtlassianJiraRestV2IssueIssueTypeResourceCreateIssueTypeAvatarPostBody:
      'any',
    ComAtlassianJiraRestV2IssueCommentPropertyResourceSetCommentPropertyPutBody:
      'any'
  },
  getBadSchemaKeys().reduce(
    (typesMap, key) => ({ ...typesMap, [key]: 'any' }),
    {}
  )
)

const parameterize = (paramName, param, consumes) => {
  if (param === null) {
    return {}
  }

  let enums = param.enum ? param.enum.map(JSON.stringify).join('|') : null

  let type = typesMap[param.type] || param.type

  if (type === 'array') {
    type = `${param.itemsType || 'string'}[]`
  }

  return {
    name: paramName,
    required: param.required,
    type: enums || type
  }
}

const generateTypes = (languageName, templateFile, outputFile, typesBlob) => {
  let typesPath = path.resolve('src', outputFile)
  let templatePath = path.resolve('scripts/templates', templateFile)

  let template = readFileSync(templatePath, 'utf8')

  let apis = Object.entries(ROUTES).reduce((apis, [apiName, apiObj]) => {
    let apiParamsName = pascalCase(`${apiName}`)

    let params = _.toPairs(apiObj.params).reduce(
      (params, [paramName, param]) =>
        params.concat(parameterize(paramName, param, apiObj.consumes)),
      []
    )

    if (apiObj.bodyType) {
      params.push({
        name: 'body',
        required: true,
        type: `JIRA.Schema.${pascalCase(
          typesMap[apiObj.bodyType] || apiObj.bodyType
        )}`
      })
    }

    let hasParams = params.length > 0

    let paramsType = hasParams ? apiParamsName : pascalCase('Empty')

    let responseType = pascalCase(
      typesMap[apiObj.responseType] || apiObj.responseType || 'Any'
    )

    return apis.concat({
      name: _.camelCase(apiName),
      paramsType,
      params: { items: params },
      exclude: !hasParams,
      responseType
    })
  }, [])

  let typesContent = Mustache.render(template, {
    apis,
    typesBlob
  })

  writeFileSync(typesPath, typesContent, 'utf8')
}

module.exports = generateTypes
