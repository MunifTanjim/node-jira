const upperFirst = require('lodash/upperFirst')
const camelCase = require('lodash/camelCase')

const pascalCase = string => upperFirst(camelCase(string))

const getBadSchemaKeys = () => {
  /**
   * IssueBean -> OpsbarBean -> LinkGroupBean -> LinkGroupBean
   * LinkGroupBean -> LinkGroupBean
   * NotificationEventBean -> NotificationEventBean
   * NotificationSchemeBean -> NotificationSchemeEventBean -> NotificationEventBean -> NotificationEventBean
   * NotificationSchemeEventBean -> NotificationEventBean -> NotificationEventBean
   * OpsbarBean -> LinkGroupBean -> LinkGroupBean
   * PageBeanNotificationSchemeBean -> NotificationSchemeBean -> NotificationSchemeEventBean -> NotificationEventBean -> NotificationEventBean
   * SearchResultsBean -> IssueBean -> OpsbarBean -> LinkGroupBean -> LinkGroupBean
   */

  let badSchemaKeys = [
    'IssueBean',
    'LinkGroupBean',
    'NotificationEventBean',
    'NotificationSchemeBean',
    'NotificationSchemeEventBean',
    'OpsbarBean',
    'PageBeanNotificationSchemeBean',
    'SearchResultsBean'
  ]

  return badSchemaKeys
}

module.exports = {
  camelCase,
  pascalCase,
  getBadSchemaKeys
}
