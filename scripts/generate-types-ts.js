const { compile } = require('json-schema-to-typescript')

const { getBadSchemaKeys } = require('./helpers.js')
const generateTypes = require('./generate-types.js')

const typeDefs = require('./type-defs.json')

const deleteBadSchemas = typeDefs => {
  getBadSchemaKeys().forEach(key => {
    delete typeDefs.properties[key]
    delete typeDefs.definitions[key]
  })
}

const formatInterfaces = typesBlob => {
  let [, , ...otherInterfaces] = typesBlob
    .trim()
    .split('export')
    .map(item => `  export ${item.trim()}`)

  otherInterfaces = otherInterfaces.map(otherInterface =>
    otherInterface.split('\n').join('\n    ')
  )

  return `${otherInterfaces.join('\n  ')}`
}

deleteBadSchemas(typeDefs)

compile(typeDefs, 'RootInterfaceToDiscard', { bannerComment: false })
  .then(formatInterfaces)
  .then(typesBlob => {
    return generateTypes(
      'TypeScript',
      'index.d.ts.mustache',
      'index.d.ts',
      typesBlob
    )
  })
  .catch(err => console.error(err))
