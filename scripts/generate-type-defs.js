const _ = require('lodash')
const path = require('path')
const { writeFileSync } = require('fs')

const deepsort = require('deep-sort-object')
const deepmerge = require('../src/utils/deepmerge.js')

const { pascalCase } = require('./helpers.js')

const SCHEMAS = require('../specification/schemas.json')
const PATHS_SPEC = require('../specification/paths.json')

const responseTypeDefsPath = path.resolve('scripts/type-defs.json')

let noiseKeys = [
  'description',
  'format',
  'minimum',
  'minItems',
  'pattern',
  'readOnly',
  'uniqueItems'
]

const deleteNoises = object => {
  _.each(noiseKeys, noise => delete object[noise])
}

const recursivelyDeleteNoises = object => {
  deleteNoises(object)
  object.items && recursivelyDeleteNoises(object.items)
  object.properties && _.each(object.properties, recursivelyDeleteNoises)
}

const entityNames = _.chain(SCHEMAS)
  .keys()
  .uniq()
  .sort()
  .value()

const rootObject = {
  type: 'object',
  definitions: SCHEMAS,
  properties: {}
}

_.each(SCHEMAS, (entity, entityName) => {
  recursivelyDeleteNoises(entity)
  rootObject.properties[entityName] = deepmerge({}, entity)
})

let stringifiedPathsSpec = JSON.stringify(PATHS_SPEC)

_.each(entityNames, entityName => {
  if (
    RegExp(`"#/components/.+?/${entityName.replace(/\./g, '\\.')}"`).test(
      stringifiedPathsSpec
    )
  ) {
    rootObject.properties[entityName] = {
      $ref: `#/definitions/${entityName}`
    }
  }
})

let stringifiedTypeDefs = JSON.stringify(deepsort(rootObject), null, 2)

_.each(entityNames, entityName => {
  stringifiedTypeDefs = _.chain(stringifiedTypeDefs)
    .replace(
      RegExp(`^( +?)"${entityName.replace(/\./g, '\\.')}":`, 'gm'),
      `$1"${pascalCase(entityName)}":`
    )
    .replace(
      RegExp(`"#/components/.+?/${entityName.replace(/\./g, '\\.')}"`, 'g'),
      `"#/definitions/${pascalCase(entityName)}"`
    )
    .value()
})

writeFileSync(responseTypeDefsPath, `${stringifiedTypeDefs}\n`)
