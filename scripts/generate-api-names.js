const _ = require('lodash')
const path = require('path')
const { writeFileSync } = require('fs')
const deepsort = require('deep-sort-object')

const API_NAMES = require('../routes/api-names.json')
const PATHS_SPEC = require('../specification/paths.json')

const apiNamesPath = path.resolve('routes/api-names.json')

const NON_METHODS = ['parameters']

_.keys(PATHS_SPEC).forEach(url => {
  if (!API_NAMES[url]) {
    API_NAMES[url] = {}
  }

  _.keys(PATHS_SPEC[url]).forEach(method => {
    if (NON_METHODS.includes(method)) {
      return
    }

    if (!API_NAMES[url][method]) {
      API_NAMES[url][method] = PATHS_SPEC[url][method].summary || ''
    }
  })
})

writeFileSync(apiNamesPath, `${JSON.stringify(deepsort(API_NAMES), null, 2)}\n`)
